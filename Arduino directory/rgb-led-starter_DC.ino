//This works
int redPin = 6;
int greenPin = 3;
int bluePin = 5;

void setup() {
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
}
int n = 0;
int count;

void loop() {
  setColor(n,0 ,0);
  delay(20);
  setColor(0,n,0);
  delay(20); 
   setColor(0, 0, n); 
   n = n + 1;
   if(n > 255) n = 0;
   delay(20);
   Serial.print(n);
   Serial.print(" ");
   count++;
  if (count % 20 == 0)Serial.println();
  
}

void setColor(int redValue, int greenValue, int blueValue) {
  analogWrite(redPin, redValue);
  analogWrite(greenPin, greenValue);
  analogWrite(bluePin, blueValue);
}
